// registrations.readme.js
let readme = ({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) =>`# ${subGroupName} registrations

- **Topic**: ${workShopDescription}
- **Date**: ${workshopDate}
- **Landing Page**: [https://${parentGroupName}.gitlab.io/${subGroupName}/landing-page](https://${parentGroupName}.gitlab.io/${subGroupName}/landing-page)

## How to register to the workshop?

It's simple: you just need to create an issue in this project.

> the ${maxAttendees} first created issues will be kept

`

module.exports = {
  readme: readme
}

